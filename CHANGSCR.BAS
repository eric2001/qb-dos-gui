DECLARE SUB displayClock ()
DECLARE SUB delay (n%)
DECLARE SUB CheckForMonitor ()
DECLARE SUB GetMonitorType ()
DECLARE SUB CenterInWindow (row%, Left%, Right%, color1%, WindowColor%, text$)
DECLARE SUB makeWindow (Left%, Right%, Top%, Bottom%, FrameColor%, FrameType%, inside%)
DECLARE SUB center (row%, text$)
DECLARE SUB GetInput (Top%, text$, ans$, en%)
DECLARE SUB KillWindow (Left%, Right%, Top%, Bottom%, inside%)
DECLARE SUB MouseStatus (Left%, Right%, Both%, x%, y%)
DECLARE SUB initializeMouse ()
DECLARE SUB ShowMouse ()
DECLARE SUB HideMouse ()

TYPE RegType
  AX AS INTEGER
  BX AS INTEGER
  cx AS INTEGER
  dx AS INTEGER
  BP AS INTEGER
  SI AS INTEGER
  DI AS INTEGER
  FLAGS AS INTEGER
END TYPE

CONST path$ = "c:\eric"
CONST PasswordFile$ = "c:\eric\password.dat"
CONST ScreenSaverLoc$ = "c:\eric\scrsaver"
CONST calculator$ = "c:\eric\calculat.exe"
CONST TmpPath$ = "c:\eric\tmp"
CONST DatFile$ = "C:\eric\INFO.DAT"
CONST TextViewer$ = "c:\eric\txtviewr.exe"
CONST char% = 176


'  ---- This sub displays the contents of the current directory on to the
'       screen and allows the user to enter different directories.
'       LastClickedFile$ is the last file or directory that was clicked on.
'       if FileType% = 1 means a file
'       if FileType% = 2 means a dir name

REM $STATIC
REM $DYNAMIC

'  ---- Set up arrayes and initialize variables.

DIM FileName$(100), FileType$(100), DateMade$(100), timeMade$(100), WinFileName$(100), FileSize$(100)
DIM PrintFileName AS STRING * 18
Top% = 5
FileType% = 1

initializeMouse
ShowMouse
'  ---- Draw the window and the stuff in the window.
CALL makeWindow(28, 60, Top%, Top% + 18, 6, 2, 11)
CALL CenterInWindow(Top% + 1, 28, 60, 15, 11, "Select a screen saver")
COLOR 6, 11
LOCATE Top% + 2, 30: PRINT "旼컴컴컴컴컴컴컴컴컴�"
LOCATE Top% + 3, 30: PRINT "�                   �"
LOCATE Top% + 4, 30: PRINT "�                   �"
LOCATE Top% + 5, 30: PRINT "�                   �"
LOCATE Top% + 6, 30: PRINT "�                   �"
LOCATE Top% + 7, 30: PRINT "�                   �"
LOCATE Top% + 8, 30: PRINT "�                   �"
LOCATE Top% + 9, 30: PRINT "�                   �"
LOCATE Top% + 10, 30: PRINT "�                   �"
LOCATE Top% + 11, 30: PRINT "�                   �"
LOCATE Top% + 12, 30: PRINT "�                   �"
LOCATE Top% + 13, 30: PRINT "읕컴컴컴컴컴컴컴컴컴�"
LOCATE Top% + 15, 30: PRINT "旼컴컴컴컴컴컴컴컴컴컴컴컴컴커"
LOCATE Top% + 16, 30: PRINT "�                            �"
LOCATE Top% + 17, 30: PRINT "읕컴컴컴컴컴컴컴컴컴컴컴컴컴켸"
COLOR 14, 11
LOCATE Top% + 4, 53: PRINT "�袴袴�"
LOCATE Top% + 5, 53: PRINT "� ok �"
LOCATE Top% + 6, 53: PRINT "훤袴暠"
COLOR 0, 11
LOCATE Top% + 4, 58: PRINT "�"
LOCATE Top% + 5, 58: PRINT "�"
LOCATE Top% + 6, 54: PRINT "袴袴�"
COLOR 15, 11: LOCATE Top% + 5, 55: PRINT "ok"
COLOR 15, 0
LOCATE Top% + 2, 51: PRINT ""
LOCATE Top% + 13, 51: PRINT ""
COLOR 0, 11
LOCATE Top% + 2, 30: PRINT "旼컴컴컴컴컴컴컴컴컴"
LOCATE Top% + 3, 30: PRINT "�                   "
LOCATE Top% + 4, 30: PRINT "�                   "
LOCATE Top% + 5, 30: PRINT "�                   "
LOCATE Top% + 6, 30: PRINT "�                   "
LOCATE Top% + 7, 30: PRINT "�                   "
LOCATE Top% + 8, 30: PRINT "�                   "
LOCATE Top% + 9, 30: PRINT "�                   "
LOCATE Top% + 10, 30: PRINT "�                  "
LOCATE Top% + 11, 30: PRINT "�                   "
LOCATE Top% + 12, 30: PRINT "�                   "
LOCATE Top% + 15, 59: PRINT "�"
LOCATE Top% + 16, 31: PRINT "                            �"
LOCATE Top% + 17, 31: PRINT "컴컴컴컴컴컴컴컴컴컴컴컴컴컴�"
COLOR 15, 11: LOCATE Top% + 14, 30: PRINT "New Screen Saver:"

'  ---- Error handler.

ON ERROR GOTO ScrSaverError

'  ---- Display the contents of the current directory.
CHDIR ScreenSaverLoc$
Directory1$ = ScreenSaverLoc$ + "\"
text$ = "DIR " + Directory1$ + "*.exe > c:\FILETEMP.TXT"
SHELL text$


'  ---- Keep looping in this sub until the ok button is clicked.

DO




'  ---- Find the value of all1%.

  OPEN "C:\FILETEMP.TXT" FOR INPUT AS #1
  all1% = 0
  DO
    all1% = all1% + 1
    LINE INPUT #1, text$
  LOOP UNTIL EOF(1) = -1
  CLOSE #1

'  ---- Read data from the text file..

  OPEN "C:\FILETEMP.TXT" FOR INPUT AS #1
  line1% = 0
  i = 0
  DO
    line1% = line1% + 1
    LINE INPUT #1, text$

'  ---- Skip the lines that don't have file names on them.

    IF (line1% > 5) AND (line1% < all1% - 1) THEN
      i = i + 1

'  ---- The first 8 characters are the file name.  Put them into the
'       FileName$ array.

      FileName$(i) = LTRIM$(LEFT$(text$, 8))
      text$ = RIGHT$(text$, LEN(text$) - 9)
      FileType$(i) = LTRIM$(RTRIM$(LEFT$(text$, 3)))
      IF FileType$(i) <> "" THEN
        FileName$(i) = RTRIM$(FileName$(i)) + "." + LTRIM$(RTRIM$(LEFT$(text$, 3)))
      'ELSE
      '  FileType$(i) = "DIR"
      '  IF LTRIM$(RTRIM$(FileName$(i))) = "." THEN : FileName$(i) = "??"
      END IF
      text$ = RIGHT$(text$, LEN(text$) - 3)
      text$ = LTRIM$(text$)
      IF LEFT$(text$, 5) = "<DIR>" THEN
        FileName$(i) = RTRIM$(FileName$(i)) + " (DIR)"
        text$ = RIGHT$(text$, LEN(text$) - 5)
        text$ = LTRIM$(text$)
      ELSE
        space% = INSTR(text$, " ")
        FileSize$(i) = LEFT$(text$, space%)
        text$ = RIGHT$(text$, LEN(text$) - space%)
      END IF
      text$ = LTRIM$(text$)
      DateMade$(i) = LEFT$(text$, 8)
      text$ = RIGHT$(text$, LEN(text$) - 9)
      timeMade$(i) = LEFT$(text$, 6)
      timeMade$(i) = LTRIM$(timeMade$(i))
      text$ = RIGHT$(text$, LEN(text$) - 6)
      WinFileName$(i) = LTRIM$(text$)
      IF LEFT$(FileName$(i), 1) = "." THEN
        FileName$(i) = ""
        FileType$(i) = ""
        DateMade$(i) = ""
        timeMade$(i) = ""
        WinFileName$(i) = ""
        i = i - 1
      END IF
      'PRINT "FileName:" + fileName$(i)
      'PRINT "FileType:" + FileType$(i)
      'PRINT "Date:" + DateMade$(i)
      'PRINT "Time:" + timeMade$(i)
      'PRINT "Windows Name:" + WinFileName$(i)
    END IF
LOOP UNTIL EOF(1) = -1 OR i = 100
LastRecord% = i
CLOSE #1
KILL "c:\FILETEMP.TXT"

x = 2
i = 0
DO
  i = i + 1
  x = x + 1
  PrintFileName = FileName$(i)
  LOCATE Top% + x, 32: PRINT PrintFileName
LOOP UNTIL i = 10
TopFile% = 1

DO
  a$ = INKEY$

  displayClock
  COLOR 15, 11
  CALL MouseStatus(Left%, Right%, Both%, Mx%, My%)

  IF Left% = 1 THEN
    StartTime$ = TIME$
    IF My% = 8 THEN
      FOR a = 31 TO 49'first item in dir thats on screen
       IF a = Mx% THEN : RowClick% = 1
      NEXT a
    ELSEIF My% = 9 THEN
        FOR a = 31 TO 49'second item in dir thats on screen
          IF a = Mx% THEN : RowClick% = 2
        NEXT a
    ELSEIF My% = 10 THEN
        FOR a = 31 TO 49'third item in dir thats on screen
          IF a = Mx% THEN : RowClick% = 3
        NEXT a
    ELSEIF My% = 11 THEN
        FOR a = 31 TO 49'fourth item in dir thats on screen
          IF a = Mx% THEN : RowClick% = 4
        NEXT a

    ELSEIF My% = 12 THEN
        FOR a = 31 TO 49'fifth item in dir thats on screen
          IF a = Mx% THEN : RowClick% = 5
        NEXT a
    ELSEIF My% = 13 THEN
        FOR a = 31 TO 49'sixth item in dir thats on screen
          IF a = Mx% THEN : RowClick% = 6
        NEXT a
    ELSEIF My% = 14 THEN
        FOR a = 31 TO 49'seventh item in dir thats on screen
          IF a = Mx% THEN : RowClick% = 7
        NEXT a
    ELSEIF My% = 15 THEN
        FOR a = 31 TO 49'eighth item in dir thats on screen
          IF a = Mx% THEN : RowClick% = 8
        NEXT a
    ELSEIF My% = 16 THEN
        FOR a = 31 TO 49'ninth item in dir thats on screen
          IF a = Mx% THEN : RowClick% = 9
        NEXT a
    ELSEIF My% = 17 THEN
        FOR a = 31 TO 49'tenth item in dir thats on screen
          IF a = Mx% THEN : RowClick% = 10
        NEXT a

    END IF

    IF My% = 9 THEN
      IF Mx% = 53 OR Mx% = 54 OR Mx% = 55 OR Mx% = 56 OR Mx% = 57 OR Mx% = 58 THEN 'click on ok
        a$ = "e"
      END IF
    ELSEIF My% = 10 THEN
      IF Mx% = 53 OR Mx% = 54 OR Mx% = 55 OR Mx% = 56 OR Mx% = 57 OR Mx% = 58 THEN 'click on ok
        a$ = "e"
      END IF
    ELSEIF My% = 11 THEN
      IF Mx% = 53 OR Mx% = 54 OR Mx% = 55 OR Mx% = 56 OR Mx% = 57 OR Mx% = 58 THEN 'click on ok
        a$ = "e"
      END IF
    END IF
  END IF
  IF (a$ = CHR$(0) + CHR$(80) OR (Left% = 1 AND Mx% = 51 AND My% = 18)) AND TopFile% < (LastRecord% - 9) THEN
    StartTime$ = TIME$
    x = 2
    i = TopFile%
    DO
      i = i + 1
      x = x + 1
      PrintFileName = FileName$(i)
      LOCATE Top% + x, 32: PRINT PrintFileName
    LOOP UNTIL x = 12
    TopFile% = TopFile% + 1
  ELSEIF (a$ = CHR$(0) + CHR$(72) OR (Left% = 1 AND Mx% = 51 AND My% = 7)) AND TopFile% > 1 THEN
    StartTime$ = TIME$
    TopFile% = TopFile% - 1
    x = 2
    i = TopFile% - 1
    DO
      i = i + 1
      x = x + 1
      PrintFileName = FileName$(i)
      LOCATE Top% + x, 32: PRINT PrintFileName
    LOOP UNTIL x = 12
  END IF
'FileType%=1 means a file
'FileType%=2 means a dir name

  IF RowClick% = 1 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile%)
    END IF
    IF FileType$(TopFile%) = "DIR" THEN
      IF FileType% = 2 THEN
        LastClickedFile$ = FileName$(TopFile%)
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile%), LEN(FileName$(TopFile%)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile%), LEN(FileName$(TopFile%)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF

  ELSEIF RowClick% = 2 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile% + 1)
    END IF
    IF FileType$(TopFile% + 1) = "DIR" THEN
      IF FileType% = 2 THEN
        LastClickedFile$ = FileName$(TopFile% + 1)
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile% + 1), LEN(FileName$(TopFile% + 1)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile% + 1), LEN(FileName$(TopFile% + 1)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF
  ELSEIF RowClick% = 3 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile% + 2)
    END IF
    IF FileType$(TopFile% + 2) = "DIR" THEN
      IF FileType% = 2 THEN
        LastClickedFile$ = FileName$(TopFile% + 2)
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile% + 2), LEN(FileName$(TopFile% + 2)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile% + 2), LEN(FileName$(TopFile% + 2)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF
  ELSEIF RowClick% = 4 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile% + 3)
    END IF
    IF FileType$(TopFile% + 3) = "DIR" THEN
      IF FileType% = 2 THEN
        LastClickedFile$ = FileName$(TopFile% + 3)
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile% + 3), LEN(FileName$(TopFile% + 3)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile% + 3), LEN(FileName$(TopFile% + 3)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF
  ELSEIF RowClick% = 5 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile% + 4)
    END IF
    IF FileType$(TopFile% + 4) = "DIR" THEN
      IF FileType% = 2 THEN
        LastClickedFile$ = FileName$(TopFile% + 4)
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile% + 4), LEN(FileName$(TopFile% + 4)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile% + 4), LEN(FileName$(TopFile% + 4)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF
  ELSEIF RowClick% = 6 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile% + 5)
    END IF
    IF FileType$(TopFile% + 5) = "DIR" THEN
      IF FileType% = 2 THEN
        LastClickedFile$ = FileName$(TopFile% + 5)
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile% + 5), LEN(FileName$(TopFile% + 5)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile% + 5), LEN(FileName$(TopFile% + 5)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF
  ELSEIF RowClick% = 7 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile% + 6)
    END IF
    IF FileType$(TopFile% + 6) = "DIR" THEN
      IF FileType% = 2 THEN
        LastClickedFile$ = FileName$(TopFile% + 6)
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile% + 6), LEN(FileName$(TopFile% + 6)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile% + 6), LEN(FileName$(TopFile% + 6)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF
  ELSEIF RowClick% = 8 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile% + 7)
    END IF
    IF FileType$(TopFile% + 7) = "DIR" THEN
      IF FileType% = 2 THEN
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
        LastClickedFile$ = FileName$(TopFile% + 7)
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile% + 7), LEN(FileName$(TopFile% + 7)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile% + 7), LEN(FileName$(TopFile% + 7)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF
  ELSEIF RowClick% = 9 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile% + 8)
    END IF
    IF FileType$(TopFile% + 8) = "DIR" THEN
      IF FileType% = 2 THEN
        LastClickedFile$ = FileName$(TopFile% + 8)
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile% + 8), LEN(FileName$(TopFile% + 8)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile% + 8), LEN(FileName$(TopFile% + 8)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF
  ELSEIF RowClick% = 10 THEN
    StartTime$ = TIME$
    IF FileType% = 1 THEN
      LastClickedFile$ = FileName$(TopFile% + 9)
    END IF
    IF FileType$(TopFile% + 9) = "DIR" THEN
      IF FileType% = 2 THEN
        LastClickedFile$ = FileName$(TopFile% + 9)
        LOCATE Top% + 16, 31: PRINT "                            "
        LOCATE Top% + 16, 31: PRINT LastClickedFile$
      ELSE
        LastClickedFile$ = ""
      END IF
      CHDIR Directory1$ + LEFT$(FileName$(TopFile% + 9), LEN(FileName$(TopFile% + 9)) - 6)
      Directory1$ = Directory1$ + LEFT$(FileName$(TopFile% + 9), LEN(FileName$(TopFile% + 9)) - 6) + "\"
      a$ = "CD"
    ELSE
      LOCATE Top% + 16, 31: PRINT "                            "
      LOCATE Top% + 16, 31: PRINT LastClickedFile$
    END IF
  END IF
  RowClick% = 0
  LOOP UNTIL a$ = "e"
  ERASE FileName$, FileType$, DateMade$, timeMade$, WinFileName$, FileSize$
  DIM FileName$(100), FileType$(100), DateMade$(100), timeMade$(100), WinFileName$(100), FileSize$(100)
  StartTime$ = TIME$
LOOP UNTIL a$ = "e"
COLOR 0, 11
LOCATE Top% + 4, 53: PRINT "�袴袴�"
LOCATE Top% + 5, 53: PRINT "� ok �"
LOCATE Top% + 6, 53: PRINT "훤袴暠"
COLOR 14, 11
LOCATE Top% + 4, 58: PRINT "�"
LOCATE Top% + 5, 58: PRINT "�"
LOCATE Top% + 6, 54: PRINT "袴袴�"
COLOR 15, 11: LOCATE Top% + 5, 55: PRINT "ok"
delay 15
COLOR 14, 11
LOCATE Top% + 4, 53: PRINT "�袴袴�"
LOCATE Top% + 5, 53: PRINT "� ok �"
LOCATE Top% + 6, 53: PRINT "훤袴暠"
COLOR 0, 11
LOCATE Top% + 4, 58: PRINT "�"
LOCATE Top% + 5, 58: PRINT "�"
LOCATE Top% + 6, 54: PRINT "袴袴�"
COLOR 15, 11: LOCATE Top% + 5, 55: PRINT "ok"
delay 10

  OPEN DatFile$ FOR INPUT AS #1
    INPUT #1, ScreenSaverName$, ver$
  CLOSE #1
  
  ScreenSaverName$ = LastClickedFile$
  IF LastClickedFile$ <> "" THEN

  OPEN DatFile$ FOR OUTPUT AS #1
    WRITE #1, ScreenSaverName$, ver$
  CLOSE #1
  makeWindow 20, 42, 10, 12, 4, 2, 9
  COLOR 15, 9: LOCATE 11, 21: PRINT "Screen Saver Changed"
  DO
    displayClock
  LOOP WHILE INKEY$ = ""
  END IF
CHDIR directory$
IF subs% = 1 THEN
ScrSaverError:
RESUME NEXT
END IF

REM $STATIC
DEFINT A-Z
SUB center (row%, text$)
'  Center text on the given row.
    colum% = LEN(text$)
    colum% = colum% / 2
    colum% = 40 - colum%
    LOCATE row%, colum%
    PRINT text$;
END SUB

DEFSNG A-Z
'DEFINT A-Z
SUB CenterInWindow (row%, Left%, Right%, color1%, WindowColor%, text$)
'  Center text in a window.
    LengthOfText% = LEN(text$)
    space% = (Right% - Left%)
    colum% = ((space% / 2))
    colum% = colum% - (LengthOfText% / 2)
    colum% = (Left% + colum%)
    LOCATE row%, colum%
    COLOR color1%, WindowColor%: PRINT text$

END SUB

DEFINT A-Z
SUB delay (n)
     n! = n / 100
     IF n! < 0 THEN n! = -n!
     x! = TIMER
     WHILE TIMER < x! + n!
          IF n > 0 THEN
               IF INKEY$ <> "" THEN EXIT SUB
          END IF
     WEND
END SUB

DEFSNG A-Z
SUB displayClock
ON LOCAL ERROR GOTO ErrorTrapTime
  COLOR 2, 0
'  LOCATE 1, 1: PRINT "Date: "; DATE$
  hour$ = (LEFT$(TIME$, 2))
  hour% = VAL(hour$)
  IF hour% >= 13 THEN
    hour% = hour% - 12
    DisTime$ = STR$(hour%) + RIGHT$(TIME$, 6)
    LOCATE 1, 63: PRINT "Time:  "; DisTime$; "pm" + CHR$(char%);
  ELSE
    LOCATE 1, 63: PRINT "Time:  "; TIME$; "am" + CHR$(char%);
  END IF
IF subs% = 1 THEN
ErrorTrapTime:
CLS
PRINT "Error number "; ERR; " occured while updating clock.  The program will now end."
END
END IF

END SUB

SUB FullScreen
'  ---- This sub program will maximaze the window and not let it be changed.
DIM inregs AS RegType, outregs AS RegType
inregs.AX = &H1200
inregs.AX = &H94
CALL interrupt(&H10, inregs, outregs)
END SUB

SUB GetInput (Top%, text$, ans$, en%)
CALL makeWindow(20, 60, Top%, Top% + 5, 6, 2, 11)
CALL CenterInWindow(Top% + 1, 2, 78, 6, 11, text$)
ans$ = ""
star$ = ""
x = Top% + 3
y = 31
LOCATE Top% + 2, 30: PRINT "旼컴컴컴컴컴컴컴컴컴�"
LOCATE Top% + 3, 30: PRINT "�                   �"
LOCATE Top% + 4, 30: PRINT "읕컴컴컴컴컴컴컴컴컴�"
COLOR 0, 11
LOCATE Top% + 2, 30: PRINT "旼컴컴컴컴컴컴컴컴컴"
LOCATE Top% + 3, 30: PRINT "�                   "
LOCATE Top% + 4, 30: PRINT ""

DO
   KeyPress$ = INKEY$
   COLOR 15, 11
   IF KeyPress$ <> "" THEN
     IF KeyPress$ = CHR$(8) THEN      'If the delete key is pressed
       IF ans$ > "" THEN
         ans$ = LEFT$(ans$, LEN(ans$) - 1)
         star$ = LEFT$(star$, LEN(star$) - 1)
         IF LEN(ans$) < 19 THEN
           LOCATE x, y: PRINT "                   "
           IF en% = 1 THEN
             LOCATE x, y: PRINT ans$;
           ELSE
             LOCATE x, y: PRINT star$
           END IF
         ELSE
           IF en% = 1 THEN
             LOCATE x, y: PRINT RIGHT$(ans$, 19)
           ELSE
             LOCATE x, y: PRINT RIGHT$(star$, 19)
           END IF
         END IF
       ELSE
         BEEP
       END IF
     ELSEIF LEN(ans$) < 19 AND KeyPress$ <> CHR$(13) THEN
       ans$ = ans$ + KeyPress$
       star$ = star$ + "*"
       IF en% = 1 THEN
         LOCATE x, y: PRINT ans$
       ELSE
         LOCATE x, y: PRINT star$
       END IF
     ELSEIF KeyPress$ <> CHR$(13) THEN
       ans$ = ans$ + KeyPress$
       star$ = star$ + "*"
       IF en% = 1 THEN
         LOCATE x, y: PRINT RIGHT$(ans$, 19)
       ELSE
         LOCATE x, y: PRINT RIGHT$(star$, 19)
       END IF
     END IF
   END IF
LOOP UNTIL KeyPress$ = CHR$(13)

END SUB

SUB HideMouse
DIM inregs AS RegType, outregs AS RegType
inregs.AX = 2
CALL interrupt(&H33, inregs, outregs)
END SUB

SUB initializeMouse
DIM inregs AS RegType, outregs AS RegType
inregs.AX = 0
CALL interrupt(&H33, inregs, outregs)

END SUB

SUB KillWindow (Left%, Right%, Top%, Bottom%, inside%)


  horiz% = Right% - Left% + 3
  BoxColor$ = STRING$(horiz%, " ")
  FOR Vert% = Top% TO Bottom%
    COLOR 15, 0: LOCATE Vert%, Left%: PRINT BoxColor$;
  NEXT Vert%

END SUB

SUB makeWindow (Left%, Right%, Top%, Bottom%, FrameColor%, FrameType%, inside%)
'  ---- This sub draws a window on the screen.

IF FrameType% = 1 THEN

'  ---- Draw the four corners.

  COLOR FrameColor%, inside%
  LOCATE Top%, Left%: PRINT CHR$(201);
  LOCATE Top%, Right%: PRINT CHR$(187);
  LOCATE Bottom%, Left%: PRINT CHR$(200);
  LOCATE Bottom%, Right%: PRINT CHR$(188);

' ---- Draw the vertical lines.

  FOR Vert% = Top% + 1 TO Bottom% - 1
    LOCATE Vert%, Left%: PRINT CHR$(186);
    LOCATE Vert%, Right%: PRINT CHR$(186);
  NEXT Vert%

'  ----Draw the horizonal lines.

  horiz% = Right% - Left% - 1
  hline$ = STRING$(horiz%, 205)
  LOCATE Top%, Left% + 1: PRINT hline$;
  LOCATE Bottom%, Left% + 1: PRINT hline$;

'  ---- Draw the inside of the window.

  horiz% = Right% - Left% - 1
  BoxColor$ = STRING$(horiz%, 32)
  FOR Vert% = Top% + 1 TO Bottom% - 1
    LOCATE Vert%, Left% + 1: PRINT BoxColor$;
  NEXT Vert%

'  ---- Draw the shadow on the bottom of the window.

  COLOR 8, 0
  FOR Vert% = Left% + 1 TO Right% + 1
    a% = SCREEN(Bottom% + 1, Vert%)
    LOCATE Bottom% + 1, Vert%: PRINT CHR$(a%);
  NEXT Vert%

'  ---- Draw the shadow on the right side of the window.

  FOR Vert% = Top% + 1 TO Bottom%
    a% = SCREEN(Vert%, Right% + 1)
    LOCATE Vert%, Right% + 1: PRINT CHR$(a%);
  NEXT Vert%

ELSEIF FrameType% = 2 THEN

'  ---- Draw the four corners.

  COLOR 0, inside%
  LOCATE Top%, Left%: PRINT "�";
  COLOR FrameColor%, inside%
  LOCATE Top%, Right%: PRINT "�";
  LOCATE Bottom%, Left%: PRINT "�";
  LOCATE Bottom%, Right%: PRINT "�";

' ---- Draw the vertical lines.

  FOR Vert% = Top% + 1 TO Bottom% - 1
    COLOR 0, inside%: LOCATE Vert%, Left%: PRINT "�";
    COLOR FrameColor%, inside%: LOCATE Vert%, Right%: PRINT "�";
  NEXT Vert%

'  ----Draw the horizonal lines.

  horiz% = Right% - Left% - 1
  hline$ = STRING$(horiz%, "�")
  COLOR 0, inside%: LOCATE Top%, Left% + 1: PRINT hline$;
  COLOR FrameColor%, inside%: LOCATE Bottom%, Left% + 1: PRINT hline$;

'  ---- Draw the inside of the window.

  horiz% = Right% - Left% - 1
  BoxColor$ = STRING$(horiz%, 32)
  FOR Vert% = Top% + 1 TO Bottom% - 1
    LOCATE Vert%, Left% + 1: PRINT BoxColor$;
  NEXT Vert%
  COLOR 15, 0

'  ---- Window shadow on the bottom.

  COLOR 8, 0
  FOR Vert% = Left% + 1 TO Right% + 1
    a% = SCREEN(Bottom% + 1, Vert%)
    LOCATE Bottom% + 1, Vert%: PRINT CHR$(a%);
  NEXT Vert%

'  ---- Window shadow on the right side.

  COLOR 8, 0
  FOR Vert% = Top% + 1 TO Bottom%
    a% = SCREEN(Vert%, Right% + 1)
    LOCATE Vert%, Right% + 1: PRINT CHR$(a%);
  NEXT Vert%

END IF

END SUB

SUB MouseStatus (Left%, Right%, Both%, x%, y%)

DIM inregs AS RegType, outregs AS RegType
Left% = 0
Right% = 0
Both% = 0
x% = 0
y% = 0
inregs.AX = 5
CALL interrupt(&H33, inregs, outregs)

IF outregs.BX = 1 THEN
  Left% = 1
ELSEIF outregs.BX = 2 THEN
  Right% = 1
ELSEIF outregs.BX = 3 THEN
  Both% = 1
END IF

x% = INT(outregs.cx / 8) + 1
y% = INT(outregs.dx / 8) + 1

END SUB

SUB RestoreScreen (FileName$)
'*******************************************************************************
'*  Restores a screen saved by SaveScreen()                                    *
'*******************************************************************************

'   display controller port
    ScreenType& = PEEK(&H63) + PEEK(&H64) * 256

'   mono or color?
    IF ScreenType& = &H3B4 THEN
       ScreenType& = &HB000                       '  mono
    ELSE
       ScreenType& = &HB800                       '  color
    END IF

'   restore screen from disk
    DEF SEG = ScreenType&
    BLOAD FileName$, 0
    DEF SEG

    END SUB

SUB SaveScreen (FileName$)


'*******************************************************************************
'*  saves current text screen to the specified binary file. If file already    *
'*  exists, it will be overwritten.  SCREEN 0 (text mode) only                 *
'*******************************************************************************

'   display controller port
    ScreenType& = PEEK(&H63) + PEEK(&H64) * 256

'   mono or color?
    IF ScreenType& = &H3B4 THEN    ' mono
       ScreenType& = &HB000
    ELSE
       ScreenType& = &HB800        ' color
    END IF

'   save screen to disk
    DEF SEG = ScreenType&
    BSAVE FileName$, 0, 4000
    DEF SEG

    END SUB

SUB ShowMouse
DIM inregs AS RegType, outregs AS RegType
inregs.AX = 1
CALL interrupt(&H33, inregs, outregs)

END SUB

SUB StatusWin (y%, FirstRun%, PrintFirst%, PrintOn%, message$, CurrPoint%, total%, doneMessage$)
      IF total% > 59 THEN
        FirstPrint! = total% / 30
        IF INSTR(STR$(FirstPrint!), ".") = 0 THEN 'total is divisable by 30
          IF FirstRun% = 1 THEN
            makeWindow 20, 52, 13, 15, 6, 1, 12
            CenterInWindow 13, 20, 50, 15, 12, message$
            PrintFirst% = total% / 30'value for when to start printing
            PrintOn% = PrintFirst%'current posisition of when to print next block
            y% = 21
          END IF

          PrintNum% = total% / PrintFirst% 'total number of blocks to print, should be 30.
          IF CurrPoint% = PrintOn% THEN
            'print a block
            y% = y% + 1
            LOCATE 14, y%: PRINT CHR$(177)
            delay 1.5
            'PRINT "printing a block"; PrintOn%
            PrintOn% = PrintOn% + PrintFirst%
          END IF
          '**********
        ELSE 'total is not divisable by 30.
          IF FirstRun% = 1 THEN
            makeWindow 20, 52, 13, 15, 6, 1, 12
            CenterInWindow 13, 20, 50, 15, 12, message$
            PrintFirst! = total% / 30'value for when to start printing
            a% = INSTR(STR$(PrintFirst!), ".")
            PrintFirst% = VAL(LEFT$(STR$(PrintFirst!), a%))
            PrintOn% = PrintFirst%'current posisition of when to print next block
            y% = 21
          END IF
          PrintNum% = total% / PrintFirst% 'total number of blocks to print, should be 30.
          IF CurrPoint% = PrintOn% THEN
            'print a block
            y% = y% + 1
            LOCATE 14, y%: PRINT CHR$(177)
            delay 1.5
            'PRINT "printing a block"; PrintOn%
            PrintOn% = PrintOn% + PrintFirst%
          END IF

        END IF
      END IF

      IF total% <= 59 THEN  ' if total value is equal to or less then the ammount of blcoks to be printed,
        IF CurrPoint% = total% THEN  'print the entire thing when its finished.
          makeWindow 20, 52, 13, 15, 6, 1, 12
          CenterInWindow 13, 20, 50, 15, 12, message$
          y = 21
          DO
            y = y + 1
            LOCATE 14, y: PRINT CHR$(177)
            LOCATE 1, 1: PRINT y - 21
            delay 1.5
          LOOP UNTIL y = 51              'loops 30 times
        END IF
      END IF

                     EXIT SUB

      IF PrintFirst% = CurrPoint% THEN 'print first block
        y = 21
        LOCATE 14, y: PRINT CHR$(177)

      END IF
      EXIT SUB
      LOCATE 2, 1: PRINT PrintFirst%
      makeWindow 20, 52, 13, 15, 6, 1, 12
      CenterInWindow 13, 20, 50, 15, 12, message$
      y = 21
      DO
        y = y + 1
        LOCATE 14, y: PRINT CHR$(177)
        LOCATE 1, 1: PRINT y - 21
        delay 1
      LOOP UNTIL y = 51              'loops 30 times

END SUB

