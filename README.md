# QB DOS GUI
![QB DOS GUI](./Qbdosmainscreen.jpg) 

## Description
This program was created back when Windows 3.1 took "forever" to load on a 20 MHz 386 CPU. It's purpose was to provide a semi-graphical interface to use when I didn't feel like waiting for Windows to load.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
Run the setup program. It'll create the necessary folders and files along with a default login account. Program will be installed to "c:\eric\", then run "main.exe" to start the program.

## History
(Dates are estimates based on File Creation and Last Modified dates and may be inaccurate)

I believe this was started sometime in 1998

This was last updated in early 2001.

Download: [Binary](./bin/qbkrnlsetup.zip) | [Source](./bin/qbkrnlsource.zip)
